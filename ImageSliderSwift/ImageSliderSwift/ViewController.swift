//
//  ViewController.swift
//  ImageSliderSwift
//
//  Created by VKGraphics on 09/04/17.
//  Copyright © 2017 VKGraphics. All rights reserved.
//

import UIKit
import ImageSlideshow
class ViewController: UIViewController {

    @IBOutlet var imageSlider: ImageSlideshow!
    let localSource = [ImageSource(imageString: "img1")!, ImageSource(imageString: "img2")!, ImageSource(imageString: "img3")!, ImageSource(imageString: "img4")!]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageSlider.backgroundColor = UIColor.white
        imageSlider.slideshowInterval = 5.0
        imageSlider.pageControlPosition = PageControlPosition.underScrollView
        imageSlider.pageControl.currentPageIndicatorTintColor = UIColor.lightGray
        imageSlider.pageControl.pageIndicatorTintColor = UIColor.black
        imageSlider.contentScaleMode = UIViewContentMode.scaleAspectFill

        

        imageSlider.setImageInputs(localSource)
        
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(ViewController.didTap))
        imageSlider.addGestureRecognizer(recognizer)
        
    }
    
    func didTap() {
        imageSlider.presentFullScreenController(from: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

